﻿using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace Api1.Modules.Persian.Utils {
    public struct PersianDateTime : IComparable, IEquatable<PersianDateTime>,
       IFormattable, IComparable<PersianDateTime>,
       ISerializable {
        private PersianCalendar pc;
        private System.DateTime _datetime;

        private int _year;

        public int Year {
            get { return _year; }
            set {
                int val = value;
                if (val < 0 || val < 1300) throw new ArgumentOutOfRangeException();

                _year = val;

            }
        }

        private int _month;

        public int Month {
            get { return _month; }
            set {
                int val = value;
                if (val < 1 || val > 12) throw new ArgumentOutOfRangeException();

                _month = val;
            }
        }

        private int _day;

        public int Day {
            get { return _day; }
            set {
                int val = value;

                if ((Month >= 1 && Month <= 6) && val < 1 || val > 31 ||
                    (Month >= 7 && Month <= 11) && val < 1 || val > 30 ||
                    (Month == 12 && pc.IsLeapYear(Year) && val < 1 || val > 30) ||
                    (Month == 12 && !pc.IsLeapYear(Year) && val < 1 || val > 29))
                    throw new ArgumentOutOfRangeException();

                _day = val;
            }
        }


        private int _hour;

        public int Hour {
            get { return _hour; }
            set {
                int val = value;

                if (val < 0 || val > 23) throw new ArgumentOutOfRangeException();

                _hour = val;
            }
        }


        private int _minute;

        public int Minute {
            get { return _minute; }
            set {
                int val = value;

                if (val < 0 || val > 59) throw new ArgumentOutOfRangeException();

                _minute = val;
            }
        }

        private int _second;

        public int Second {
            get { return _second; }
            set {
                int val = value;

                if (val < 0 || val > 59) throw new ArgumentOutOfRangeException();

                _second = val;
            }
        }


        public PersianDateTime(System.DateTime dt) {
            pc = new PersianCalendar();

            _datetime = dt;

            _year = pc.GetYear(_datetime);
            _month = pc.GetMonth(_datetime);
            _day = pc.GetDayOfMonth(_datetime);

            _hour = pc.GetHour(_datetime);
            _second = pc.GetMinute(dt);
            _minute = pc.GetSecond(dt);

        }


        public PersianDateTime(PersianDateTime pdt) {
            pc = new PersianCalendar();
            _year = pdt.Year;
            _month = pdt.Month;
            _day = pdt.Day;
            _hour = pdt.Hour;
            _minute = pdt.Minute;
            _second = pdt.Second;

            _datetime = pdt._datetime;
        }


        public PersianDateTime(int year, int month, int day)
            : this(year, month, day, 0, 0, 0) {
        }


        public PersianDateTime(int year, int month, int day, int hour, int minute, int second) {
            pc = new PersianCalendar();

            _year = year;
            _month = month;
            _day = day;

            _hour = hour;
            _minute = minute;
            _second = second;

            _datetime = pc.ToDateTime(year, month, day, hour, minute, second, 0);
        }


        public static PersianDateTime Now {
            get {
                return new PersianDateTime(System.DateTime.Now);
            }
        }


        public override bool Equals(object obj) {
            return obj is PersianDateTime &&
                   CompareTo(obj) == 0;
        }

        public override int GetHashCode() {
            return ToLongDateTime().GetHashCode();
        }

        public override string ToString() {
            return Year.ToString("0000") + "/" +
                   Month.ToString("00") + "/" +
                   Day.ToString("00");
        }

        public string ToString(string format) {
            // YMDHms  1378/01/22 13:45:30
            // yMDHms  78/01/22   13:45:30
            // yMDhms  78/01/22   01:45:30
            // A ق. ب.  ب. ظ. 

            string rv = "";
            char[] rx = rv.ToCharArray();

            foreach (char ch in rx) {
                switch (ch) {
                    case 'Y': rv += Year.ToString("0000"); break;
                    case 'y': rv += Year.ToString("00"); break;
                    case 'M': rv += Month.ToString("00"); break;
                    case 'D': rv += Day.ToString("00"); break;
                    case 'H': rv += Hour.ToString("00"); break;
                    case 'h': rv += (Hour % 12).ToString("00"); break;
                    case 'm': rv += Minute.ToString("00"); break;
                    case 's': rv += Second.ToString("00"); break;
                    case 'A': rv += (Hour >= 12 ? "ب.ظ." : "ق.ظ."); break;
                    default: rv += ch; break;
                }
            }

            return rv;
        }



        public string ToShortDateTime() {
            return this.ToString();
        }


        public string ToLongDateTime() {
            return Year.ToString("0000") + "/" +
                   Month.ToString("00") + "/" +
                   Day.ToString("00") + " " +
                   Hour.ToString("00") + ":" +
                   Minute.ToString("00") + ":" +
                   Second.ToString("00");
        }

        public System.DateTime ToDateTime() {
            return _datetime;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context) {
            throw new NotImplementedException();
        }

        public int CompareTo(object obj) {
            //throw new NotImplementedException();
            int rv = -999;

            if (obj is PersianDateTime) {
                PersianDateTime pd = (PersianDateTime)obj;

                rv = Year > pd.Year ? 1 : Year < pd.Year ? -1 : 0;
                rv = (rv == 0) ? Month > pd.Month ? 1 : Month < pd.Month ? -1 : 0 : rv;
                rv = (rv == 0) ? Day > pd.Day ? 1 : Day < pd.Day ? -1 : 0 : rv;
                rv = (rv == 0) ? Hour > pd.Hour ? 1 : Hour < pd.Hour ? -1 : 0 : rv;
                rv = (rv == 0) ? Minute > pd.Minute ? 1 : Minute < pd.Minute ? -1 : 0 : rv;
                rv = (rv == 0) ? Second > pd.Second ? 1 : Second < pd.Second ? -1 : 0 : rv;

            }
            else {
                throw new InvalidCastException();
            }

            return rv;
        }


        bool IEquatable<PersianDateTime>.Equals(PersianDateTime other) {
            return this.Equals(other);
        }


        public int CompareTo(PersianDateTime other) {
            return this.CompareTo((object)other);
        }


        public string ToString(string format, IFormatProvider formatProvider) {
            throw new NotImplementedException();
        }

        public static bool operator ==(PersianDateTime dt1, System.DateTime dt2) {
            return (dt1 == new PersianDateTime(dt2));
        }

        public static bool operator ==(System.DateTime dt1, PersianDateTime dt2) {
            return (new PersianDateTime(dt1) == dt2);
        }


        public static bool operator !=(System.DateTime dt1, PersianDateTime dt2) {
            return !(dt1 == dt2);
        }


        public static bool operator !=(PersianDateTime dt1, System.DateTime dt2) {
            return !(dt1 == dt2);
        }

        public static bool operator ==(PersianDateTime dt1, PersianDateTime dt2) {
            return dt1.CompareTo(dt2) == 0;
        }

        public static bool operator >(PersianDateTime dt1, PersianDateTime dt2) {
            return dt1.CompareTo(dt2) > 0;
        }

        public static bool operator >=(PersianDateTime dt1, PersianDateTime dt2) {
            return dt1.CompareTo(dt2) >= 0;
        }

        public static bool operator <(PersianDateTime dt1, PersianDateTime dt2) {
            return dt1.CompareTo(dt2) < 0;
        }

        public static bool operator <=(PersianDateTime dt1, PersianDateTime dt2) {
            return dt1.CompareTo(dt2) <= 0;
        }

        public static bool operator !=(PersianDateTime dt1, PersianDateTime dt2) {
            return !dt1.Equals(dt2);
        }

        public static PersianDateTime Parse(string date) {
            // 1378/7/7 12:23:45
            // *    * *  *  *  *
            string[] rv = date.Split(new char[] { '/', ':', ' ' });
            int[] val = new int[7] { 1388,01,01,
                0,0,0,0 };
            int i = 0;

            foreach (string v in rv)
                if (i < 7) val[i++] = Int32.Parse(v);


            return new PersianDateTime(val[0], val[1], val[2], val[3], val[4], val[5]);
        }


        string IFormattable.ToString(string format, IFormatProvider formatProvider) {
            //throw new NotImplementedException();
            return this.ToString();
        }


        public static bool TryParse(string date) {
            bool rv = false;

            try {
                PersianDateTime.Parse(date);
                rv = true;
            }
            catch {
                rv = false;
            }

            return rv;
        }

    }
}
